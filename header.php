<html>
<html <?php language_attributes()?>>

<head>
    <meta charset="<?php bloginfo('charset')?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php bloginfo('description')?>">
    <title>
    <?php  bloginfo('name'); ?>
</title>
<?php  wp_head(); ?>
<link href='https://fonts.googleapis.com/css?family=Poppins:200' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
<link rel="stylesheet" href='<?php bloginfo('template_url')?>/sass-files/style.css' type='text/css' media='all' />
</head>

<body>
    <div class="titulo">
        <h1>Roberto Rosa</h1>
    </div>