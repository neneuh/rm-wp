<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package tto-dev
 */

?>

		<?php the_title( '<h1>', '</h1>' ); ?>

		<?php
		the_content();

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'tto-dev' ),
			'after'  => '</div>',
		) );
		?>
<!-- .entry-content -->

<!-- #post-<?php the_ID(); ?> -->
