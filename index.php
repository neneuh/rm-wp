<?php get_header()?>
    <div class="main-grid">
            <?php 			
			wp_nav_menu( array(
                'menu_id' => 'menu',
                'menu_class' => 'mimenu'
			) );
            ?>
            <div class="main-content">
                <p>Front-end: <br>
                    CSS3, Javascript, ES6, React, ThreeJS, DataViz
                </p>
                <p>Back-end<br>
                NodeJS, PHP, APIs, SQL, NoSQL
                </p>
                <p>CMS<br>
                    Wordpress, Drupal
                </p>
                <p>Frameworks - Full stack<br>
                    Laravel, Angular</p>
                <p>Cloud<br>
                    AWS, DO, MongoDB Atlas
                </p>
            </div>
    <?php get_footer(); ?>